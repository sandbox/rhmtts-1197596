<?php

/**
 * @file
 *
 * A test script you can use to test if this module does indeed send out
 * email. Run using drush php-script
 *
 * Don't forget to set a sender and a recipient
 */

$sender = 'test@example.com';
$recipient = 'test@example.com';
$subject = 'Test';

$template_path = drupal_get_path('module', 'ica_email_from_template') . '/theme/demo.tpl.php';

$template_vars = array(
  'a' => array('b' => 'c'),
  'd' => array(1, 2, 3, 4),
  'e' => array('f' => 'g', 'h' => 'i')
);


ica_email_from_template_send($sender, $recipient, $subject, $template_path, $template_vars);
