<?php
/**
 * @file
 * The template that includes the real template that will be parsed and sent.
 */

/**
 *
 * DO NOT MODIFY THIS TEMPLATE
 * This template merely includes to the actual requested template.
 *
 */

?>
<?php
if (! $full_template_path ) {
  drupal_set_message(t("No template set for email from template call"), "error");
}
else {
  if (! file_exists($full_template_path) ) {
    drupal_set_message(t('Non-existent template path "@template" requested in email from template call', array('@template' => $full_template_path)), 'error');
  }
  else {
    include($full_template_path);
  };
}

