<?php

/**
 * @file
 * Example template to send out via email.
 */

?>

<h1>ICA Email From Template Demo</h1>

<p>
The variables array <em>$template_vars</em> contains the following items:
</p>

<?php
$header = array('Key', 'Value');
$rows = array();
foreach ($template_vars as $key => $value) {
  $rows[] = array(
    $key,
    '<pre>' . print_r($value, TRUE) . '</pre>',
  );
};
echo theme_table($header, $rows);


